# micro

A modern and intuitive terminal-based text editor

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/micro.git
```

https://github.com/zyedidia/micro
